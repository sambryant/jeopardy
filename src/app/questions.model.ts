export type QuestionType = 'text' | 'picture' | 'video';

export type QuestionState = 'fresh' | 'displayed' | 'finished';

export interface Category {
  title: string;
  type: QuestionType;
  example: Question;
  questions: Question[];
}

export interface Round {
  values: number[];
  categories: Category[];
}

export interface Player {
  name: string;
  score: number;
  questions: Question[];
}

export class Question {
  value: number;
  question: string;
  answer: string;
  finished: boolean;
  category: string;
  state: QuestionState;
  type: QuestionType;

  constructor(question: string, answer: string, value: number, type: QuestionType, category: string) {
    this.question = question;
    this.answer = answer;
    this.value = value;
    this.type = type;
    this.category = category;
    this.state = 'fresh';
    this.finished = false;
  }

}

export function makeNew(name: string): Player {
  return {
    name: name,
    score: 0,
    questions: []
  };
}
