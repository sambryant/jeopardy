import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SoundsService {

  private correct;
  private incorrect;

  constructor() {
    this.correct = new Audio();
    this.correct.src = "/assets/sounds/right.oga";
    this.correct.load();
    this.incorrect = new Audio();
    this.incorrect.src = "/assets/sounds/wrong.ogg";
    this.incorrect.load();
  }

  wrong() {
    this.incorrect.play();
  }

  right() {
    this.correct.play();
  }
}
