import {
  AfterViewInit,
  ViewChild,
  Component
} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Round, Player, makeNew, QuestionType, QuestionState, Question, Category} from './questions.model';
import {StopwatchComponent} from './stopwatch/stopwatch.component';
import {SoundsService} from './sounds.service';

export interface RawCategory {
  title: string;
  ignored: boolean;
  type: QuestionType;
  exampleQuestion?: string;
  exampleAnswer?: string;
  questions: string[][];
}

export interface QuestionsResponse {
  roundOne: RawCategory[];
  roundTwo: RawCategory[];
  finalCategory: string;
  finalQuestion: string;
}

export interface PlayersResponse {
  names: string[];
  passOrder: number[][];
}

export type GameState = 'not-started' | 'round-1' | 'round-2' | 'final';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  @ViewChild('stopwatch')
  stopwatch: StopwatchComponent;

  // categoryPresentationIndex = 0;

  roundOne: Round;
  roundTwo: Round;
  finalCategory: string;
  finalQuestion: string;
  players: Player[] = null;

  gameState: GameState = 'not-started';

  roundPassOrder: number[][];

  currentQuestion: Question = null;

  turnInd = 0;

  passInd = 0;

  lastPass = -1; // index of last player who answered current question.

  nextPass = 0;

  passCount = 0;

  // List of booleans indicating if player has answered already this turn
  playerAnswered: boolean[];

  round: number = 0;

  private computeNextPass() {
    const relativeNextPass = this.roundPassOrder[this.round][this.passCount];
    this.nextPass = (this.turnInd + relativeNextPass) % this.players.length;
  }

  // nextPresentedCategory() {
  //   if (this.categoryPresentationIndex + 1 === this.roundOne.categories.length) {
  //     this.gameState = 'round-1';
  //     this.categoryPresentationIndex = 0;
  //   } else {
  //     this.categoryPresentationIndex++;
  //   }
  // }

  startRoundOne() {
    // this.gameState = 'categories-1';
    this.gameState = 'round-1';
  }

  startRoundTwo() {
    this.gameState = 'round-2';
  }

  startFinal() {
    this.gameState = 'final';
  }

  nextTurn() {
    const n = this.players.length;

    this.currentQuestion.finished = true;

    this.turnInd++;
    this.passCount = 0;
    this.playerAnswered = this.players.map(_ => false);

    if (this.turnInd == this.players.length) {
      this.turnInd = 0;
      this.round++;
    }
    this.computeNextPass();
    this.lastPass = -1;
    this.passInd = this.turnInd;
    this.currentQuestion = null;
  }

  back() {
    if (this.passCount === 0) {
      return;
    }

    this.passCount -= 1;
    this.nextPass = this.passInd;
    this.passInd = this.lastPass;
    this.lastPass = -1;
    this.playerAnswered[this.passInd] = false;
  }

  pass() {
    this.soundsService.wrong();

    const n = this.players.length;

    this.playerAnswered[this.passInd] = true;

    if (this.passCount + 1 == n) {
      this.nextTurn();
    } else {
      this.lastPass = this.passInd;
      this.passInd = this.nextPass;
      this.passCount++;
      this.computeNextPass();
    }
    this.stopwatch.clear();
  }

  correct() {
    this.soundsService.right();
    const p = this.players[this.passInd];
    p.questions.push(this.currentQuestion);
    p.score += this.currentQuestion.value;
    this.nextTurn();
    this.stopwatch.clear();
  }

  onStarted(question: Question) {
    this.currentQuestion = question;
  }

  private buildRound(rawCategories: [], values: number[]): Round {
    const categories: Category[] = rawCategories
      .filter((category: RawCategory) => !category.ignored)
      .map((category: RawCategory) => {
        let cat = {
          title: category.title,
          type: category.type,
          questions: values.map((value, index) => {
            let q = 'filler';
            let a = 'filler';
            if (category.questions[index]) {
              q = category.questions[index][0];
              a = category.questions[index][1];
            }
            return new Question(q, a, value, category.type, category.title);
          }),
          example: new Question(category.exampleQuestion, category.exampleAnswer, 0, category.type, category.title)
        };
        return cat;
    });
    return {values, categories};
  }

  private analyzeOrder() {
    const nRounds = 2;
    const nPlayers = this.players.length;

    let passing = this.players.map(_ => {
      return this.players.map(_ => {
        return 0
      });
    });

    let passOrder = this.players.map(_ => {return 0;});
    let passCount = this.players.map(_ => {return 0;});

    let subround = 0;
    while (subround < 12) {
      this.players.forEach((p: Player, index: number) => {
        let i = 0;
        while (i < 4) {
          const passesTo = (index + this.roundPassOrder[subround][i]) % this.players.length;
          passOrder[passesTo] += (i + 1);
          passCount[passesTo] += 1;
          i++;
        }

        const passesTo = (index + this.roundPassOrder[subround][0]) % this.players.length;
        passing[index][passesTo] += 1;
      });
      subround += 1;
    }

    let avgPassOrder = this.players.map((_: Player, index: number) => {
      return passOrder[index] / passCount[index];
    });
    console.log(avgPassOrder);
    console.log(passCount);
    console.log(passing);
  }

  constructor(public soundsService: SoundsService, private http: HttpClient) {
    this.http.get<PlayersResponse>('http://localhost:4200/assets/players.json').subscribe((response: PlayersResponse) => {
      this.players = response.names.map(name => {
        return {
          name,
          score: 0,
          questions: []
        };
      });
      this.roundPassOrder = response.passOrder;
      this.playerAnswered = this.players.map(_ => false);
      this.computeNextPass();

      this.http.get<QuestionsResponse>('http://localhost:4200/assets/questions.json').subscribe((response: any) => {
        this.finalCategory = response.finalCategory;
        this.finalQuestion = response.finalQuestion;
        this.roundOne = this.buildRound(response.roundOne, [100, 200, 300, 400, 500]);
        this.roundTwo = this.buildRound(response.roundTwo, [200, 400, 600, 800, 1000]);

        this.analyzeOrder();
      });
    });
  }
}
